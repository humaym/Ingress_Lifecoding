package Ingress.exercise.service;

import Ingress.exercise.dto.*;
import Ingress.exercise.model.Organization;
import Ingress.exercise.model.Task;
import Ingress.exercise.model.User;
import Ingress.exercise.repository.OrganizationRepository;
import Ingress.exercise.repository.TaskRepository;
import Ingress.exercise.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final ModelMapper modelMapper;

    public TaskResponse create(Long orgId, TaskRequest request) {
        Organization org = organizationRepository.findById(orgId).orElseThrow();
        Task task = modelMapper.map(request, Task.class);
        task.setOrganization(org);
        taskRepository.save(task);
        org.getTasks().add(task);
        organizationRepository.save(org);
        return modelMapper.map(task, TaskResponse.class);
    }

    public TaskResponse update(Long orgId, Long taskId, TaskRequest request) {
        Task task = taskRepository.findTaskByOrgId(orgId, taskId);
        task.setName(request.getName());
        task.setDeadline(request.getDeadline());
        task.setDescription(request.getDescription());
        task.setStatus(request.getStatus());
        taskRepository.save(task);
        return modelMapper.map(task, TaskResponse.class);
    }

    public TaskResponse get(Long taskId) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new RuntimeException(String.format("Task with id %s not found", taskId)));
        return modelMapper.map(task, TaskResponse.class);
    }

    public List<TaskResponse> getAll() {
        List<Task> tasks = taskRepository.findAll();
        List<TaskResponse> taskResponses = tasks.stream()
                .map(task -> modelMapper.map(task, TaskResponse.class))
                .collect(Collectors.toList());
        return taskResponses;
    }

    public void delete(Long taskId) {
        taskRepository.deleteById(taskId);
    }

    public TaskResponse addUsersToTask(Long taskId, List<Long> userIds) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new RuntimeException(String.format("Task with id %s not found", taskId)));
        List<User> users = userRepository.findAllById(userIds);
        task.getUsers().addAll(users);
        taskRepository.save(task);
        return modelMapper.map(task, TaskResponse.class);
    }
}
