package Ingress.exercise.service;

import Ingress.exercise.dto.UserRequest;
import Ingress.exercise.dto.UserResponse;
import Ingress.exercise.model.Organization;
import Ingress.exercise.model.Task;
import Ingress.exercise.model.User;
import Ingress.exercise.repository.OrganizationRepository;
import Ingress.exercise.repository.TaskRepository;
import Ingress.exercise.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;

    public UserResponse create(Long orgId, UserRequest request) {
        Organization org = organizationRepository.findById(orgId)
                .orElseThrow(() -> new RuntimeException(String.format("Organization with id %s not found", orgId)));
        User user = modelMapper.map(request, User.class);
        if (!request.getPassword().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{10,}$")) {
            throw new RuntimeException("Password must have at least 10 characters, one uppercase letter, one lowercase letter, and one digit");
        }
        String encodedPassword = passwordEncoder.encode(request.getPassword());
        user.setPassword(encodedPassword);
        user.setOrganization(org);
        org.getUsers().add(user);
        userRepository.save(user);
        organizationRepository.save(org);
        return modelMapper.map(user, UserResponse.class);
    }

    public UserResponse update(Long orgId, Long userId, UserRequest request) {
        User user = userRepository.findUserByOrgId(orgId, userId);
        user.setEmail(request.getEmail());
        user.setName(request.getName());
        user.setSurname(request.getSurname());
        if (!request.getPassword().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{10,}$")) {
            throw new RuntimeException("Password must have at least 10 characters, one uppercase letter, one lowercase letter, and one digit");
        }
        String encodedPassword = passwordEncoder.encode(request.getPassword());
        user.setPassword(encodedPassword);
        user.setPassword(encodedPassword);
        userRepository.save(user);
        return modelMapper.map(user, UserResponse.class);
    }

    public UserResponse get(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException(String.format("User with id %s not found", userId)));
        return modelMapper.map(user, UserResponse.class);
    }

    public List<UserResponse> getAll() {
        List<User> users = userRepository.findAll();
        List<UserResponse> userResponses = users.stream()
                .map(user -> modelMapper.map(user, UserResponse.class))
                .collect(Collectors.toList());
        return userResponses;
    }

    public void delete(Long userId) {
        userRepository.deleteById(userId);
    }

}

