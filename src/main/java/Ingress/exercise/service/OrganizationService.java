package Ingress.exercise.service;

import Ingress.exercise.dto.OrganizationRequest;
import Ingress.exercise.dto.OrganizationResponse;
import Ingress.exercise.dto.TaskRequest;
import Ingress.exercise.dto.TaskResponse;
import Ingress.exercise.model.Organization;
import Ingress.exercise.model.Task;
import Ingress.exercise.repository.OrganizationRepository;
import Ingress.exercise.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.tuple.CreationTimestampGeneration;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final ModelMapper modelMapper;

    public OrganizationResponse create(OrganizationRequest request) {
        Organization org = modelMapper.map(request, Organization.class);
        organizationRepository.save(org);
        return modelMapper.map(request, OrganizationResponse.class);
    }

    public OrganizationResponse update(Long orgId, OrganizationRequest request) {
        Organization org = organizationRepository.findById(orgId)
                .orElseThrow(() -> new RuntimeException(String.format("Organization with id %s not found", orgId)));
        org.setName(request.getName());
        organizationRepository.save(org);
        return modelMapper.map(org, OrganizationResponse.class);
    }

    public void delete(Long orgId) {
        organizationRepository.deleteById(orgId);
    }

    public OrganizationResponse get(Long orgId) {
        Organization org = organizationRepository.findById(orgId)
                .orElseThrow(() -> new RuntimeException(String.format("Organization with id %s not found", orgId)));
        return modelMapper.map(org, OrganizationResponse.class);
    }

    public List<OrganizationResponse> getAll() {
        List<Organization> orgs = organizationRepository.findAll();
        List<OrganizationResponse> organizationResponses = orgs.stream()
                .map(org -> modelMapper.map(org, OrganizationResponse.class))
                .collect(Collectors.toList());
        return organizationResponses;
    }
}
