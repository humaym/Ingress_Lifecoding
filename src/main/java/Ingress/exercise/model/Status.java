package Ingress.exercise.model;

public enum Status {
    ACTIVE,
    TODO,
    DONE
}
