package Ingress.exercise.repository;

import Ingress.exercise.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TaskRepository extends JpaRepository<Task,Long> {
   @Query(value = "select * from task t where t.organization_id = :orgId and t.id= :taskId",nativeQuery = true)
    Task findTaskByOrgId(Long orgId,Long taskId);
}
