package Ingress.exercise.repository;

import Ingress.exercise.model.Task;
import Ingress.exercise.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User,Long> {

    @Query(value = "select * from user u where u.organization_id = :orgId and u.id= :userId",nativeQuery = true)
    User findUserByOrgId(Long orgId, Long userId);
}
