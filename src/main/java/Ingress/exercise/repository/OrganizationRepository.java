package Ingress.exercise.repository;

import Ingress.exercise.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface OrganizationRepository extends JpaRepository<Organization,Long> {
    @Query(value = "select o from Organization o join fetch o.tasks t join fetch o.users u")
    Optional<Organization> getAll();
}
