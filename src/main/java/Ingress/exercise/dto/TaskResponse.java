package Ingress.exercise.dto;

import Ingress.exercise.model.Status;
import Ingress.exercise.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskResponse {
    Long id;

    String name;
    LocalDate deadline;

    @Enumerated(EnumType.STRING)
    Status status;
    String description;
     List<User> users;
}
