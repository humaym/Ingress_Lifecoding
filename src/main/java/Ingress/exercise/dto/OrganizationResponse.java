package Ingress.exercise.dto;

import Ingress.exercise.model.Task;
import Ingress.exercise.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationResponse {
    Long id;
    String name;
    List<Task> tasks;
    List<User> users;
}
