package Ingress.exercise;

import Ingress.exercise.repository.TaskRepository;
import Ingress.exercise.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class ExerciseApplication implements CommandLineRunner {

    private final TaskRepository taskRepository;
	private final UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(ExerciseApplication.class, args);
    }

	@Override
	@Transactional
	public void run(String... args) throws Exception {
//		System.out.println(taskRepository.findTaskByOrgId(1L,3L));
	}
}
