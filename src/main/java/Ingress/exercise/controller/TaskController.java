package Ingress.exercise.controller;

import Ingress.exercise.dto.*;
import Ingress.exercise.service.OrganizationService;
import Ingress.exercise.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @PostMapping("/org/{orgId}")
    public TaskResponse create(@PathVariable long orgId, @RequestBody TaskRequest request) {
        return taskService.create(orgId, request);
    }

    @PutMapping("/org/{orgId}/task/{taskId}")
    public TaskResponse update(@PathVariable Long orgId, @PathVariable Long taskId, @RequestBody TaskRequest request) {
        return taskService.update(orgId, taskId, request);
    }

    @DeleteMapping("/{taskId}")
    public void delete(@PathVariable Long taskId) {
        taskService.delete(taskId);
    }

    @GetMapping("/{taskId}")
    public TaskResponse get(@PathVariable Long taskId) {
        return taskService.get(taskId);
    }

    @GetMapping
    public List<TaskResponse> getAll() {
        return taskService.getAll();
    }

    @PostMapping("/{taskId}/users")
    public TaskResponse addUsersToTask(@PathVariable Long taskId, @RequestBody List<Long> userIds) {
        return taskService.addUsersToTask(taskId, userIds);
    }
}
