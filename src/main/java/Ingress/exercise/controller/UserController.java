package Ingress.exercise.controller;

import Ingress.exercise.dto.TaskRequest;
import Ingress.exercise.dto.TaskResponse;
import Ingress.exercise.dto.UserRequest;
import Ingress.exercise.dto.UserResponse;
import Ingress.exercise.service.TaskService;
import Ingress.exercise.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/org/{orgId}")
    public UserResponse create(@PathVariable long orgId, @RequestBody UserRequest request) {
        return userService.create(orgId, request);
    }

    @PutMapping("/org/{orgId}/user/{userId}")
    public UserResponse update(@PathVariable Long orgId, @PathVariable Long userId, @RequestBody UserRequest request) {
        return userService.update(orgId, userId, request);
    }

    @DeleteMapping("/{userId}")
    public void delete(@PathVariable Long userId) {
        userService.delete(userId);
    }

    @GetMapping("/{userId}")
    public UserResponse get(@PathVariable Long userId) {
        return userService.get(userId);
    }

    @GetMapping
    public List<UserResponse> getAll() {
        return userService.getAll();
    }
}
