package Ingress.exercise.controller;

import Ingress.exercise.dto.OrganizationRequest;
import Ingress.exercise.dto.OrganizationResponse;
import Ingress.exercise.service.OrganizationService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/org")
@RequiredArgsConstructor

public class OrganizationController {

    private final OrganizationService organizationService;

    @PostMapping
    public OrganizationResponse create(@RequestBody OrganizationRequest request) {
        return organizationService.create(request);
    }

    @PutMapping("/{orgId}")
    public OrganizationResponse update(@PathVariable Long orgId, @RequestBody OrganizationRequest request) {
        return organizationService.update(orgId, request);
    }

    @DeleteMapping("/{orgId}")
    public void delete(@PathVariable Long orgId) {
        organizationService.delete(orgId);
    }

    @GetMapping("/{orgId}")
    public OrganizationResponse get(@PathVariable Long orgId) {
        return organizationService.get(orgId);
    }

    @GetMapping
    public List<OrganizationResponse> getAll() {
        return organizationService.getAll();
    }
}
